import { ALL_FARMS, Farm } from 'models/Farm';
import createStore from 'teaful';
import { getFarmsFromPics } from 'utils/getFarmsFromPics';
import isSameArray from 'utils/IsSameArray';

interface AppState {
  status: 'ready' | 'loading' | 'error' | 'loaded';
  error: string | null;
  pics: string[];
  selectedPics: string[];
  farms: Farm[];
  selectedFarms: Farm[];
  filter: {
    farmId: string;
    query: string;
  };
  showDetailView: boolean;
  deforestationMonths: number;
}

const initialState: AppState = {
  status: 'ready',
  error: null,
  pics: [],
  selectedPics: [],
  farms: [],
  selectedFarms: [],
  filter: {
    farmId: ALL_FARMS._id,
    query: '',
  },
  showDetailView: false,
  deforestationMonths: 12,
};

export const { useStore, getStore, setStore, withStore } = createStore<AppState>(
  initialState,
  onAfterUpdate,
);

export const resetStore = () => {
  setStore(initialState);
};

function onAfterUpdate({ store }: { store: AppState }) {
  // Update selected farms
  const selectedFarms =
    store.selectedPics.length === 0
      ? store.farms
      : getFarmsFromPics(store.farms, store.selectedPics);

  if (!isSameArray<Farm>(selectedFarms, store.selectedFarms, '_id')) {
    setStore.selectedFarms(selectedFarms);
  }
}
