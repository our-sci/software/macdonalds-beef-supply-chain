import { useCallback, useMemo } from 'react';
import { useStore } from 'config/store';
import { ChartData } from 'models/ChartData';
import { BIODIVERSITY_HABITATS, BIODIVERSITY_PRACTICES } from 'utils/constants';
import { getLabel } from 'utils/getLabel';

export const useBiodiversity = () => {
  const [farms] = useStore.farms();
  const [selectedFarms] = useStore.selectedFarms();

  const protectPercent = useMemo<number>(() => {
    const totalCount = farms.length;
    const protectedCount = selectedFarms.filter((farm) =>
      farm.answer.biodiversityPracticeActive.includes('habitat_restoration'),
    ).length;

    return totalCount === 0 ? 0 : Math.round((protectedCount * 100) / totalCount);
  }, [farms.length, selectedFarms]);

  const habitats = useMemo(
    () =>
      BIODIVERSITY_HABITATS.map((habitat) => {
        const totalCount = farms.length;
        const habitatsCount = selectedFarms.filter((farm) =>
          farm.answer.biodiversityHabitats.includes(habitat),
        ).length;

        return {
          habitats: getLabel(habitat),
          percent: totalCount === 0 ? 0 : Math.round((habitatsCount * 100) / totalCount),
        };
      }).sort((a, b) => b.percent - a.percent),
    [farms.length, selectedFarms],
  );

  const getChartData = useCallback(
    (): ChartData[] =>
      BIODIVERSITY_PRACTICES.map((practice) => ({
        practice,
        active: selectedFarms.filter((farm) =>
          farm.answer.biodiversityPracticeActive.includes(practice),
        ).length,
        interest: selectedFarms.filter((farm) =>
          farm.answer.biodiversityPracticeInterest.includes(practice),
        ).length,
      })),
    [selectedFarms],
  );

  return {
    protectPercent,
    habitats,
    getChartData,
  };
};
