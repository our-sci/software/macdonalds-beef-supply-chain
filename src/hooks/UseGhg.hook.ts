import { useCallback, useMemo } from 'react';
import { useStore } from 'config/store';
import { ChartData } from 'models/ChartData';
import { GHG_PRACTICES } from 'utils/constants';

export const useGgh = () => {
  const [farms] = useStore.farms();
  const [selectedFarms] = useStore.selectedFarms();

  const neutral = useMemo<number>(() => {
    const totalCount = farms.length;
    const neutralCount = selectedFarms.filter((farm) => farm.answer.ghgCalculated <= 0).length;

    return totalCount === 0 ? 0 : Math.round((neutralCount * 100) / totalCount);
  }, [farms.length, selectedFarms]);

  const getChartData = useCallback(
    (): ChartData[] =>
      GHG_PRACTICES.map((practice) => ({
        practice,
        active: selectedFarms.filter((farm) => farm.answer.ghgPracticeActive.includes(practice))
          .length,
        interest: selectedFarms.filter((farm) => farm.answer.ghgPracticeInterest.includes(practice))
          .length,
      })),
    [selectedFarms],
  );

  const getBoxPlot = useCallback(
    (
      height: number,
      tickWidth: number,
      borderWidth: number,
    ): {
      min: number;
      max: number;
      average: number;
      minY: number;
      maxY: number;
      averageY: number;
    } => {
      const values = selectedFarms.map((farm) => farm.answer.ghgCalculated);
      const min = Math.min(...values);
      const max = Math.max(...values);
      const diff = max - min;
      const sum = values.reduce((acc, cur) => acc + cur, 0);
      const average = values.length === 0 ? 0 : sum / values.length;
      const maxY = 0;
      const minY = height - tickWidth;
      const averageY = diff === 0 ? (minY - maxY) / 2 : ((max - average) * (minY - maxY)) / diff;

      return {
        min,
        max,
        average,
        minY: minY - borderWidth,
        maxY: maxY - borderWidth,
        averageY: averageY - borderWidth,
      };
    },
    [selectedFarms],
  );

  return {
    neutral,
    getChartData,
    getBoxPlot,
  };
};
