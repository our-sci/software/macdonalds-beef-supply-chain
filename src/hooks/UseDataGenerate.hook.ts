import { useCallback } from 'react';
import { resetStore, useStore } from 'config/store';
import { Farm } from 'models/Farm';
import { getFarmsFromPics } from 'utils/getFarmsFromPics';

export const useDataGenerate = () => {
  const [, setStatus] = useStore.status();
  const [, setError] = useStore.error();
  const [, setPics] = useStore.pics();
  const [, setFarms] = useStore.farms();

  const fetchData = useCallback(
    async (pics?: string[]) => {
      if (pics && pics.length === 0) {
        setStatus('error');
        setError('No valid PICs found');
        return;
      }

      let uniquePics = [...new Set(pics)];
      if (uniquePics.length > 0) {
        console.info('PICs are generated', uniquePics);
        setPics(uniquePics);
      }

      await fetch(import.meta.env.VITE_API_URL)
        .then((res) => res.json())
        .then((data) => {
          const farms = (data as Farm[]).map((farm) => ({
            ...farm,
            pic: [...new Set(farm.pic.filter((pic) => pic && pic.length === 8))],
          }));

          if (uniquePics.length === 0) {
            const randomPics = farms
              .flatMap((farm) => farm.pic)
              .sort(() => Math.random() - 0.5)
              .slice(0, 10);

            uniquePics = [...new Set(randomPics)];
            console.info('PICs are generated', uniquePics);
            setPics(uniquePics);
          }

          setFarms(getFarmsFromPics(farms, uniquePics));

          setStatus('loaded');
          setError(null);
        })
        .catch((e) => {
          setStatus('error');
          setError(e.message || String(e));
        });
    },
    [setError, setFarms, setPics, setStatus],
  );

  const parseCsv = useCallback(
    (file: File) => {
      resetStore();
      setStatus('loading');

      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        const data = String(e.target?.result || '');
        const result = data
          .replaceAll('\r\n', '\n')
          .split('\n')
          .map((row) => (row.split(',').shift() || '').trim())
          .filter((item) => item?.length === 8);
        fetchData(result);
      };
      fileReader.onerror = (e) => {
        const message = e.target?.error?.message || 'Something went wrong while parsing CSV file.';
        setStatus('error');
        setError(message);
      };
      fileReader.readAsText(file);
    },
    [fetchData, setError, setStatus],
  );

  const parseInput = useCallback(
    async (text: string) => {
      resetStore();
      setStatus('loading');

      const result = text
        .split(',')
        .map((item) => item.trim())
        .filter((item) => item.length === 8);
      await fetchData(result);
    },
    [fetchData, setStatus],
  );

  return {
    parseCsv,
    parseInput,
    fetchData,
  };
};
