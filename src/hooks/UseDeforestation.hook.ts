import { useMemo } from 'react';
import { useStore } from 'config/store';
import { Farm } from 'models/Farm';

export const useDeforestation = () => {
  const [farms] = useStore.farms();
  const [selectedFarms] = useStore.selectedFarms();
  const [months] = useStore.deforestationMonths();

  const clearedFarms = useMemo<Farm[]>(
    () => selectedFarms.filter((farm) => farm.answer.clearedLand.some((m) => m <= months)),
    [selectedFarms, months],
  );

  const clearedArea = useMemo<number>(
    () => clearedFarms.reduce((acc, cur) => acc + (cur.answer.clearedLandHectares ?? 0), 0),
    [clearedFarms],
  );

  const clearedPercent = useMemo<number>(() => {
    const totalCount = farms.length;
    const clearedCount = clearedFarms.length;

    return totalCount === 0 ? 0 : Math.round((clearedCount * 100) / totalCount);
  }, [clearedFarms.length, farms.length]);

  return {
    clearedArea,
    clearedFarms,
    clearedPercent,
  };
};
