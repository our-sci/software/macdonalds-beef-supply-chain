export const GHG_METHODS = ['self_assessment', 'online_tool', 'climate_active', 'unknown', 'other'];

export const GHG_PRACTICES = [
  'soil_conservation',
  'carbon_sequestration',
  'reduce_tillage',
  'crop_residue',
  'agroforestry',
  'cover_crop',
  'nutrient_management',
  'feed_additives',
  'rotational_grazing',
  'enteric_fermentation',
  'manure_management',
  'fuel_switching',
  'farm_energy_audit',
  'hvac_mainenance',
  'automated_hvac',
  'electic_equipment',
  'energy_production',
  'anaerobic_digestion',
  'goethermal',
  'solar_thermal',
  'wind',
  'photovoltaic',
  'battery',
];

export const BIODIVERSITY_HABITATS = [
  'endangered_community',
  'endangered_habitat',
  'woodland',
  'forest',
  'wetland',
  'creek',
  'river',
  'other',
];

export const BIODIVERSITY_PRACTICES = [
  'weed_pest_control',
  'fencing',
  'water_point',
  'habitat_restoration',
  'habitat_connectivity',
  'planting',
  'other  ',
];
