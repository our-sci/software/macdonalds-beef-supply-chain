/* eslint-disable @typescript-eslint/no-explicit-any */
const isSameArray = <T extends { [key: string]: any } = { [key: string]: any }>(
  aryA: T[],
  aryB: T[],
  key?: string,
): boolean => {
  if (aryA === aryB) {
    return true;
  }

  if (aryA.length !== aryB.length) {
    return false;
  }

  return key
    ? aryA.every((item, index) => item[key] === aryB[index][key])
    : aryA.every((item, index) => JSON.stringify(item) === JSON.stringify(aryB[index]));
};

export default isSameArray;
