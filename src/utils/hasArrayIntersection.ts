/* eslint-disable @typescript-eslint/no-explicit-any */
export const hasArrayIntersection = <T extends { [key: string]: any } = { [key: string]: any }>(
  ary1: T[],
  ary2: T[],
  key?: string,
): boolean => {
  if (ary1.length === 0 || ary2.length === 0) {
    return false;
  }

  const ary = ary1.length < ary2.length ? ary1 : ary2;

  return key
    ? ary.some((item1) => ary2.some((item2) => item1[key] === item2[key]))
    : ary.some((item1) => ary2.some((item2) => JSON.stringify(item1) === JSON.stringify(item2)));
};
