import { Farm } from 'models/Farm';

export const getFarmsFromPics = (farms: Farm[], pics: string[]): Farm[] =>
  farms
    .filter((farm) => farm.pic.some((pic) => pics.includes(pic)))
    .filter((farm, index, ary) => ary.findIndex((item) => item._id === farm._id) === index);
