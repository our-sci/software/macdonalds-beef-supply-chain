import React, { useEffect } from 'react';
import { HeaderView, MainView, SidebarView } from 'components/app';
import { useStore } from 'config/store';

function App() {
  const [status] = useStore.status();
  const [error] = useStore.error();

  useEffect(() => {
    const appHeight = () => {
      document.documentElement.style.setProperty('--app-height', `${window.innerHeight}px`);
    };
    window.addEventListener('resize', appHeight);
    appHeight();

    return () => {
      window.removeEventListener('resize', appHeight);
    };
  }, []);

  return (
    <main
      className="min-h-full"
      style={{
        backgroundImage: `url(${import.meta.env.BASE_URL}bg.jpg)`,
        backgroundSize: 'cover',
        backgroundPosition: '80% center',
        backgroundRepeat: 'no-repeat',
      }}
    >
      <HeaderView />

      {status === 'loaded' ? (
        <div className="relative z-0 flex h-[calc(var(--app-height)_-_165px)] items-stretch bg-white pt-2 md:h-[calc(var(--app-height)_-_120px)]">
          <MainView />
          <SidebarView />
        </div>
      ) : (
        <p className="mx-auto mt-20 w-fit px-4 text-center text-3xl text-white drop-shadow sm:mt-4 sm:text-blue-800 sm:shadow-white">
          {status === 'ready' && 'Please input PICs or upload CSV'}
          {status === 'loading' && 'Loading...'}
          {status === 'error' && (error || 'Something went wrong')}
        </p>
      )}
    </main>
  );
}

export default App;
