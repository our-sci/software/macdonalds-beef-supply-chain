export interface Pic {
  pic: string;
  lon: number;
  lat: number;
}
