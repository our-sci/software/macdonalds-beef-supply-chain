import { Answer } from './Answer';
import { Pic } from './Pic';

export interface Farm {
  _id: string;
  name: string;
  pic: string[];
  picDetails: Pic[];
  answer: Answer;
}

export const ALL_FARMS = {
  _id: '',
  name: 'All Farms',
  pic: [],
  picDetails: [],
} as unknown as Farm;
