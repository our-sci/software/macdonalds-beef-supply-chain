export interface ChartData {
  practice: string;
  active: number;
  interest: number;
}
