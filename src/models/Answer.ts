export interface Answer {
  ghgCalculated: number;
  ghgCalculatedMethod: string[];
  ghgPracticeActive: string[];
  ghgPracticeInterest: string[];
  clearedLand: number[];
  clearedLandHectares: number | null;
  clearedLandConfidence: number | null;
  biodiversityHabitats: string[];
  biodiversityPracticeActive: string[];
  biodiversityPracticeInterest: string[];
}
