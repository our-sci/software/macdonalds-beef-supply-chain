import React, { ComponentType, FC } from 'react';
import { Tab } from '@headlessui/react';
import clsx from 'clsx';
import { useStore } from 'config/store';
import { BiodiversityView, DashboardView, DeforestationView, GhgView, MapView } from './tabs';

const TABS: [string, ComponentType][] = [
  ['Dashboard', DashboardView],
  ['Map', MapView],
  ['GHG', GhgView],
  ['Deforestation', DeforestationView],
  ['Biodiversity', BiodiversityView],
];

export const MainView: FC = () => {
  const [showDetailView] = useStore.showDetailView();

  return (
    <section
      className={clsx(
        'absolute inset-0 z-[1] px-3 py-2 md:static md:px-6',
        'flex-1 flex-col items-stretch overflow-auto bg-white md:flex',
        {
          flex: showDetailView,
          hidden: !showDetailView,
        },
      )}
    >
      <Tab.Group>
        <Tab.List className="flex space-x-1 rounded-xl bg-blue-400 p-1">
          {TABS.map(([label]) => (
            <Tab
              key={label}
              className={({ selected }) =>
                clsx(
                  'w-full rounded-lg px-1 py-2.5 text-sm font-medium leading-5 text-blue-700',
                  'ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-500 focus:outline-none focus:ring-2',
                  'overflow-hidden text-ellipsis',
                  {
                    'bg-white shadow': selected,
                    'text-white/90 hover:bg-white/[0.12] hover:text-white/100': !selected,
                  },
                )
              }
            >
              {label}
            </Tab>
          ))}
        </Tab.List>
        <Tab.Panels className="mt-3 min-h-[calc(100%_-_60px)]">
          {TABS.map(([, Component], index) => (
            <Tab.Panel key={index} className="h-full">
              <Component />
            </Tab.Panel>
          ))}
        </Tab.Panels>
      </Tab.Group>
    </section>
  );
};
