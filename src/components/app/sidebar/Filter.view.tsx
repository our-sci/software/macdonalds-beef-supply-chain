import React, { ChangeEvent, FC, useMemo } from 'react';
import { Input, Select } from 'components/common';
import { useStore } from 'config/store';
import { ALL_FARMS, Farm } from 'models/Farm';

export const FilterView: FC = () => {
  const [farms] = useStore.farms();
  const [filterFarmId, setFilterFarmId] = useStore.filter.farmId();
  const [query, setQuery] = useStore.filter.query();
  const [, selectPics] = useStore.selectedPics();

  const items = useMemo<Farm[]>(() => [ALL_FARMS, ...farms], [farms]);

  const handleFarmChange = (farmId: string) => {
    setFilterFarmId(farmId);
    selectPics([]);
  };

  const handleSearchChange = (e: ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
    selectPics([]);
  };

  return (
    <div className="sticky top-0 z-[1] flex flex-col items-stretch gap-2 bg-white py-4">
      Filter by
      <Select
        items={items.map((item) => item._id)}
        value={filterFarmId}
        onChange={handleFarmChange}
        getLabel={(id) => items.find((item) => item._id === id)?.name ?? id}
      />
      <Input value={query} onChange={handleSearchChange} placeholder="Type to search" />
    </div>
  );
};
