import React, { FC } from 'react';
import { FilterView } from './Filter.view';
import { ListView } from './List.view';

export const SidebarView: FC = () => {
  return (
    <nav className="z-0 max-h-full w-full overflow-y-auto overflow-x-hidden p-3 pt-0 md:w-1/3 md:max-w-[500px]">
      <FilterView />
      <ListView />
    </nav>
  );
};
