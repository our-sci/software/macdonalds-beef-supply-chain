import React, { FC } from 'react';
import clsx from 'clsx';
import { useStore } from 'config/store';
import { Farm } from 'models/Farm';

interface ListItemProps {
  pic: string;
  farm: Farm;
}

export const ListItemView: FC<ListItemProps> = ({ pic, farm }) => {
  const [selectedPics, selectPics] = useStore.selectedPics();

  const isActive = selectedPics.includes(pic);

  const handleSelect = () => {
    if (isActive) {
      selectPics((prev) => prev.filter((item) => item !== pic));
    } else {
      selectPics((prev) => [...prev, pic]);
    }
  };

  return (
    <li
      className={clsx(
        'group flex h-10 min-h-0 shrink-0 cursor-pointer items-center justify-between rounded px-2 text-xs md:h-12 md:px-4 md:text-sm',
        {
          'bg-blue-400 text-white': isActive,
          'border border-gray-200 hover:bg-blue-100': !isActive,
        },
      )}
      onClick={handleSelect}
    >
      <span>{pic}</span>
      <span
        className={clsx({
          'max-w-[calc(100%_-_100px)] overflow-hidden text-ellipsis whitespace-nowrap text-gray-400 group-hover:text-gray-600':
            !isActive,
        })}
      >
        {farm.name}
      </span>
    </li>
  );
};
