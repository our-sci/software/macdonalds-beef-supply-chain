import React, { FC, useMemo } from 'react';
import { useStore } from 'config/store';
import { ALL_FARMS, Farm } from 'models/Farm';
import { ListItemView } from './ListItem.view';

type Row = [pic: string, farm: Farm];

export const ListView: FC = () => {
  const [pics] = useStore.pics();
  const [farms] = useStore.farms();
  const [{ farmId, query }] = useStore.filter();

  const data = useMemo<Row[]>(() => {
    const result = (farmId === ALL_FARMS._id ? farms : farms.filter((farm) => farm._id === farmId))
      .flatMap((farm) => farm.pic.map((pic) => [pic, farm] as Row))
      .filter(([pic]) => pics.includes(pic))
      .sort((a, b) => a[0].localeCompare(b[0]));

    const q = query.toLowerCase();
    return q
      ? result.filter(
          ([pic, farm]) => pic.toLowerCase().includes(q) || farm.name.toLowerCase().includes(q),
        )
      : result;
  }, [farmId, farms, pics, query]);

  if (data.length === 0) {
    return <p className="py-4 text-center">No data to show</p>;
  }

  return (
    <ul className="flex flex-col items-stretch gap-1">
      {data.map(([pic, farm]) => (
        <ListItemView key={pic} pic={pic} farm={farm} />
      ))}
    </ul>
  );
};
