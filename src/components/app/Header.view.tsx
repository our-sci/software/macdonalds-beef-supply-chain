import React, { ChangeEvent, FC, useEffect, useRef, useState } from 'react';
import clsx from 'clsx';
import { resetStore, useStore } from 'config/store';
import { useDataGenerate } from 'hooks';
import { Button, ButtonGroup, Input } from '../common';

const VIEW_MODES = ['List', 'Detail'];

export const HeaderView: FC = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const fileRef = useRef<HTMLInputElement>(null);
  const [inputVal, setInputVal] = useState<string>('');
  const [status] = useStore.status();
  const [, setDetailView] = useStore.showDetailView();
  const { parseCsv, parseInput, fetchData } = useDataGenerate();

  const toggleView = (item: string) => {
    setDetailView(item === VIEW_MODES[1]);
  };

  const handleGenerate = async () => {
    parseInput(inputVal);
  };

  const handleUpload = () => {
    fileRef.current?.click();
  };

  const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target?.files?.[0];
    e.target.value = '';
    if (file) {
      parseCsv(file);
    }
  };

  const handleDemo = () => {
    if (status === 'loaded') {
      resetStore();
    } else {
      fetchData();
    }
  };

  useEffect(() => {
    if (status === 'error') {
      setInputVal('');
      inputRef.current?.focus();
    }
  }, [status]);

  return (
    <header
      className={clsx(
        'relative z-10 min-h-[120px] p-3 lg:p-6',
        'flex flex-col items-stretch justify-center gap-3 md:flex-row md:items-center',
        {
          'bg-white shadow': status === 'loaded',
        },
      )}
    >
      <div className="flex items-center justify-between">
        <label htmlFor="picInput" className="d-label align-middle text-lg">
          Enter PICs
        </label>

        {status === 'loaded' && (
          <ButtonGroup className="md:hidden" items={VIEW_MODES} onClick={toggleView} />
        )}
      </div>
      <Input
        id="picInput"
        ref={inputRef}
        className="flex-1 md:max-w-[450px]"
        value={inputVal}
        onChange={(e) => setInputVal(e.target.value)}
        placeholder="Separate your PICs with a comma"
      />
      <div className="flex items-center gap-2 max-md:flex-wrap">
        <Button
          className="flex-1 md:flex-auto"
          onClick={handleGenerate}
          disabled={!inputVal || status === 'loading'}
        >
          Generate
        </Button>
        <Button
          className="flex-1 md:flex-auto"
          variant="outline"
          onClick={handleUpload}
          disabled={status === 'loading'}
        >
          Upload CSV
        </Button>
        <input
          ref={fileRef}
          type="file"
          className="absolute hidden h-0 w-0"
          onChange={handleFileChange}
          multiple={false}
          accept=".csv"
        />

        <Button
          className="max-md:w-full sm:flex-1 md:flex-auto"
          variant="outline"
          onClick={handleDemo}
        >
          {status === 'loaded' ? 'Reset' : 'Continue w/ demo'}
        </Button>
      </div>
    </header>
  );
};
