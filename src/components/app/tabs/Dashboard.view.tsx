import React, { FC, useMemo } from 'react';
import { Rating } from 'components/common';

const getRandomRating = (): number => Math.random() * 5;

export const DashboardView: FC = () => {
  const data = useMemo(
    () => [
      {
        label: 'Australian Agricultural Sustainability Framework',
        categories: [
          { name: 'GHG emissions', rating: getRandomRating() },
          { name: 'Air quality', rating: getRandomRating() },
          { name: 'Soil health', rating: getRandomRating() },
          { name: 'Landscape health', rating: getRandomRating() },
          { name: 'Biodiversity Health', rating: getRandomRating() },
          { name: 'Water quality', rating: getRandomRating() },
          { name: 'Finite resources', rating: getRandomRating() },
        ],
      },
      {
        label: 'Beef Sustainability',
        categories: [
          { name: 'GHG emissions', rating: getRandomRating() },
          { name: 'Deforestation', rating: getRandomRating() },
          { name: 'Biodiversity', rating: getRandomRating() },
          { name: 'Animal welfare', rating: getRandomRating() },
        ],
      },
    ],
    [],
  );

  const overallScore = useMemo(() => {
    const ratings = data.flatMap((item) => item.categories.map((c) => c.rating));
    const ratingSum = ratings.reduce((acc, cur) => acc + cur, 0);
    return ratingSum / ratings.length;
  }, [data]);

  return (
    <div className="max-w-[450px] space-y-5 p-5">
      <div className="flex items-center justify-between gap-2 text-base font-medium md:text-lg">
        Overall score
        <Rating className="text-lime-400" value={overallScore} />
      </div>

      {data.map(({ label, categories }) => (
        <div key={label} className="space-y-2">
          <p className="text-base font-medium md:text-lg">{label}:</p>
          <ul className="ml-2 space-y-2">
            {categories.map(({ name, rating }, index) => (
              <li
                key={name}
                className="flex items-center justify-between gap-2 text-sm md:text-base"
              >
                {`${index + 1}. ${name}`}
                <Rating className="text-lime-400" value={rating} />
              </li>
            ))}
          </ul>
        </div>
      ))}
    </div>
  );
};
