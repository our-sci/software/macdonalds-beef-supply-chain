import React, { FC } from 'react';
import {
  BiodiversityChartPartial,
  BiodiversityHabitatPartial,
  BiodiversityProtectPercentPartial,
} from './partials';

export const BiodiversityView: FC = () => {
  return (
    <div className="flex min-h-full w-full flex-col gap-8">
      <div className="grid grid-cols-1 gap-4 md:grid-cols-2">
        <BiodiversityProtectPercentPartial />
        <BiodiversityHabitatPartial />
      </div>

      <div className="relative min-h-[300px] flex-1">
        <BiodiversityChartPartial />
      </div>
    </div>
  );
};
