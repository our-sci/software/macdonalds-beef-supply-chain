import React, { FC, HTMLAttributes } from 'react';
import clsx from 'clsx';
import { Card } from 'components/common';
import { useGgh } from 'hooks';

const BOX_HEIGHT = 160;
const TICK_WIDTH = 8;
const BORDER_WIDTH = 2;

interface TickProps extends HTMLAttributes<HTMLDivElement> {
  label?: string;
  children?: never;
}

const Tick: FC<TickProps> = ({ label, className, ...rest }) => {
  return (
    <div
      className={clsx(
        className,
        'absolute left-1/2 h-2 w-9 -translate-x-1/2 border-2 border-black bg-green-100',
      )}
      {...rest}
    >
      {label && <p className="ml-12 -mt-2 whitespace-nowrap text-sm">{label}</p>}
    </div>
  );
};

export const GhgBoxPlotPartial: FC = () => {
  const { neutral, getBoxPlot } = useGgh();

  const { min, minY, max, maxY, average, averageY } = getBoxPlot(
    BOX_HEIGHT,
    TICK_WIDTH,
    BORDER_WIDTH,
  );

  return (
    <Card className="relative" header="On-farm emissions - CO2 per kg">
      <div
        className="relative !mt-8 ml-5 w-6 border-black bg-neutral-200"
        style={{ height: BOX_HEIGHT, borderWidth: BORDER_WIDTH }}
      >
        <div className="absolute inset-0 bg-yellow-400" style={{ top: `${neutral}%` }} />
        <Tick
          className="transition-top"
          style={{ top: minY }}
          label={`Min ${Number(min.toFixed(2))}tCo2/kg`}
        />
        <Tick
          className="!bg-lime-400"
          style={{ top: averageY }}
          label={`Avg ${Number(average.toFixed(2))}tCo2/kg`}
        />
        <Tick style={{ top: maxY }} label={`Max ${Number(max.toFixed(2))}tCo2/kg`} />
      </div>
    </Card>
  );
};
