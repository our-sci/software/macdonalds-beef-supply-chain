import React, { FC } from 'react';
import { Card } from 'components/common';
import { useStore } from 'config/store';
import { useDeforestation } from 'hooks';

export const DeforestationAreaPartial: FC = () => {
  const [months] = useStore.deforestationMonths();
  const { clearedArea } = useDeforestation();

  return (
    <Card
      header={`Within last ${months} months,\ntotal area of land cleared was`}
      content={`${clearedArea}ha of farms`}
      footer="with medium confidence"
    />
  );
};
