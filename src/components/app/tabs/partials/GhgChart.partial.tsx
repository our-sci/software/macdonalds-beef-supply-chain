import React, { FC } from 'react';
import { useGgh } from 'hooks';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Cell,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import { GHG_PRACTICES } from 'utils/constants';
import { getColor } from 'utils/getColor';
import { getLabel } from 'utils/getLabel';
import { LegendPartial } from './Legend.partial';

export const GhgChartPartial: FC = () => {
  const { getChartData } = useGgh();

  const data = getChartData();

  return (
    <div className="absolute inset-0 overflow-x-auto">
      <ResponsiveContainer minWidth={1000} width="100%" height="100%">
        <BarChart data={data} maxBarSize={30}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="practice" interval={0} tickFormatter={() => ''} />
          <YAxis />
          <Tooltip
            labelFormatter={getLabel}
            formatter={(value, name) => [
              `${value} farms`,
              `${name === 'active' ? 'Active' : 'Interest'} practices`,
            ]}
          />
          <Legend content={<LegendPartial data={GHG_PRACTICES} />} />
          <Bar dataKey="active">
            {data.map((value, index) => (
              <Cell key={index} fill={getColor(value.practice).toHexString()} />
            ))}
          </Bar>
          <Bar dataKey="interest">
            {data.map((value, index) => {
              const color = getColor(value.practice);
              const adjusted = color.isDark()
                ? color.lighten(10).toHexString()
                : color.darken(10).toHexString();

              return <Cell key={index} fill={adjusted} />;
            })}
          </Bar>
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};
