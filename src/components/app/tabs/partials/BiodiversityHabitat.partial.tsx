import React, { FC } from 'react';
import { Card } from 'components/common';
import { useBiodiversity } from 'hooks';

export const BiodiversityHabitatPartial: FC = () => {
  const { habitats } = useBiodiversity();

  return (
    <Card header="Biodiversity habitats on farms">
      <div className="flex flex-col items-end gap-2">
        {habitats.map(({ habitats, percent }) => (
          <div key={habitats} className="flex w-full items-center gap-3">
            <p
              className="flex-1 cursor-default overflow-hidden text-ellipsis whitespace-nowrap text-end text-xl"
              title={habitats}
            >
              {habitats}
            </p>
            <div className="w-12 shrink-0 rounded bg-blue-700 text-center text-base text-white">{`${percent}%`}</div>
          </div>
        ))}
      </div>
    </Card>
  );
};
