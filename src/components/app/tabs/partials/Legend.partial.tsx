import React, { FC } from 'react';
import clsx from 'clsx';
import { getColor } from 'utils/getColor';
import { getLabel } from 'utils/getLabel';

interface LegendItemProps {
  item: string;
}

const LegendItem: FC<LegendItemProps> = ({ item }) => {
  const label = getLabel(item);
  const color = getColor(item);

  return (
    <span
      className="max-w-full overflow-hidden text-ellipsis whitespace-nowrap hover:font-semibold"
      style={{
        color: color.isDark() ? color.toHexString() : color.darken(20).toHexString(),
      }}
      title={label}
    >
      <span
        className="mt-0.5 mr-1 inline-block h-3 w-3"
        style={{ backgroundColor: color.toHexString() }}
      />
      {label}
    </span>
  );
};

interface LegendPartialProps {
  data: string[];
}

export const LegendPartial: FC<LegendPartialProps> = ({ data }) => {
  return (
    <div
      className={clsx('pb-4 text-sm', {
        'grid grid-flow-col gap-x-2': data.length > 10,
        'space-x-4 text-center': data.length <= 10,
      })}
      style={{
        gridTemplateRows: `repeat(6, minmax(0, 1fr))`,
      }}
    >
      {data.map((item) => (
        <LegendItem key={item} item={item} />
      ))}
    </div>
  );
};
