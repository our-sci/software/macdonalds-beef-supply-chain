import React, { FC } from 'react';
import { Card } from 'components/common';
import { useBiodiversity } from 'hooks';

export const BiodiversityProtectPercentPartial: FC = () => {
  const { protectPercent } = useBiodiversity();

  return (
    <Card
      header={'Habitat protection or\nregeneration underway on'}
      content={`${protectPercent}% of farms`}
      footer="with medium confidence"
    />
  );
};
