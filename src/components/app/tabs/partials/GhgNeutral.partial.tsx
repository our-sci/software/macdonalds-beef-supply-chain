import React, { FC } from 'react';
import { Card } from 'components/common';
import { useGgh } from 'hooks';

export const GhgNeutralPartial: FC = () => {
  const { neutral } = useGgh();

  return (
    <Card
      header={`${neutral}% of farms are`}
      content="Carbon Neutral"
      footer="with medium confidence"
    />
  );
};
