import React, { FC, useMemo, useState } from 'react';
import { ArrowSmallDownIcon, ArrowSmallUpIcon, ChevronUpDownIcon } from '@heroicons/react/20/solid';
import clsx from 'clsx';
import { useStore } from 'config/store';
import dayjs from 'dayjs';

interface Props {
  className?: string;
}

type Header = 'pic' | 'farm' | 'area' | 'date';
type SortOrder = 'up' | 'down';
type Sort = `${Header}-${SortOrder}`;

export const DeforestationTablePartial: FC<Props> = ({ className }) => {
  const [pics] = useStore.pics();
  const [farms] = useStore.farms();
  const [selectedPics] = useStore.selectedPics();
  const [months] = useStore.deforestationMonths();
  const [sort, setSort] = useState<Sort>();

  const headerData = useMemo<{ label: string; header: Header }[]>(
    () => [
      {
        label: 'PIC',
        header: 'pic',
      },
      {
        label: 'Farm name',
        header: 'farm',
      },
      {
        label: 'Total cleared (ha)',
        header: 'area',
      },
      {
        label: 'Dates cleared',
        header: 'date',
      },
    ],
    [],
  );

  const tableData = useMemo<
    {
      pic: string;
      name: string;
      area: string;
      date: string;
    }[]
  >(() => {
    let data = pics.map((pic) => {
      let name = 'N/A';
      let area = 'N/A';
      let date = 'N/A';

      const farm = farms.find((farm) => farm.pic.includes(pic));
      if (farm?.name) {
        name = farm.name;
      }
      if (typeof farm?.answer.clearedLandHectares === 'number') {
        area = (
          farm.answer.clearedLand.some((m) => m <= months) ? farm.answer.clearedLandHectares : 0
        ).toString();
      }
      if (farm && farm.answer.clearedLand.length > 0) {
        const latest = Math.min(...farm.answer.clearedLand);
        date = dayjs().subtract(latest, 'month').format('MMMM YYYY');
      }

      return { pic, name, area, date };
    });

    if (sort?.startsWith('pic')) {
      data = data.sort((a, b) => a.pic.localeCompare(b.pic));
    } else if (sort?.startsWith('farm')) {
      data = data.sort((a, b) => a.name.localeCompare(b.name));
    } else if (sort?.startsWith('area')) {
      data = data.sort((a, b) => {
        const numA = Number(a.area);
        const numB = Number(b.area);
        if (isNaN(numA)) {
          return 1;
        }
        if (isNaN(numB)) {
          return -1;
        }
        return numA - numB;
      });
    } else if (sort?.startsWith('date')) {
      data = data.sort((a, b) => {
        const dateA = dayjs(a.date, { format: 'MMMM YYYY' });
        const dateB = dayjs(b.date, { format: 'MMMM YYYY' });
        if (!dateA.isValid()) {
          return 1;
        }
        if (!dateB.isValid()) {
          return -1;
        }
        if (dateA.isBefore(dateB, 'month')) {
          return -1;
        }
        if (dateA.isAfter(dateB, 'month')) {
          return 1;
        }
        return 0;
      });
    }

    if (sort?.endsWith('down')) {
      data = data.reverse();
    }

    return data;
  }, [farms, months, pics, sort]);

  const cellWidth = [120, undefined, 200, 160];

  const handleSort = (header: Header) => {
    if (sort === `${header}-up`) {
      setSort(`${header}-down`);
    } else if (sort === `${header}-down`) {
      setSort(undefined);
    } else {
      setSort(`${header}-up`);
    }
  };

  return (
    <div className={clsx(className, 'max-w-full overflow-x-auto')}>
      <div className="w-full min-w-[756px] overflow-hidden rounded-xl border border-green-300">
        <table className="w-full">
          <thead>
            <tr>
              {headerData.map(({ label, header }) => (
                <th
                  key={label}
                  className="group cursor-pointer select-none border-b border-green-300 bg-green-100 px-2 py-1 text-left lg:px-4 lg:py-2"
                  onClick={() => handleSort(header)}
                >
                  <div className="flex items-center justify-between">
                    {label}
                    {sort === `${header}-up` ? (
                      <ArrowSmallUpIcon className="h-6 w-6 text-gray-700" />
                    ) : sort === `${header}-down` ? (
                      <ArrowSmallDownIcon className="h-6 w-6 text-gray-700" />
                    ) : (
                      <ChevronUpDownIcon className="hidden h-4 w-4 text-gray-400 group-hover:block" />
                    )}
                  </div>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {tableData.map(({ pic, name, area, date }, index) => (
              <tr key={pic} className="even:bg-green-50">
                {[pic, name, area, date].map((val, i) => (
                  <td
                    key={i}
                    width={cellWidth[i]}
                    className={clsx('px-2 py-1 lg:px-4 lg:py-2', {
                      'border-b border-green-300': index < tableData.length - 1,
                      'font-medium': selectedPics.includes(pic),
                    })}
                  >
                    {val}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
