import React, { FC } from 'react';
import { Card } from 'components/common';
import { useStore } from 'config/store';
import { useDeforestation } from 'hooks';

export const DeforestationPercentPartial: FC = () => {
  const [months] = useStore.deforestationMonths();
  const { clearedPercent } = useDeforestation();

  return (
    <Card
      header={`Within last ${months} months,\nland has been cleared on`}
      content={`${clearedPercent}% of farms`}
      footer="with high confidence"
    />
  );
};
