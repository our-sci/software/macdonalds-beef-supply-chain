import React, { FC } from 'react';
import { GhgBoxPlotPartial, GhgChartPartial, GhgNeutralPartial } from './partials';

export const GhgView: FC = () => {
  return (
    <div className="flex min-h-full w-full flex-col gap-8">
      <div className="grid grid-cols-1 gap-4 md:grid-cols-2">
        <GhgNeutralPartial />
        <GhgBoxPlotPartial />
      </div>

      <div className="relative min-h-[400px] flex-1">
        <GhgChartPartial />
      </div>
    </div>
  );
};
