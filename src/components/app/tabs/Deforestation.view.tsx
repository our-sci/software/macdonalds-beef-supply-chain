import React, { FC, useState } from 'react';
import { Input } from 'components/common';
import { useStore } from 'config/store';
import {
  DeforestationAreaPartial,
  DeforestationPercentPartial,
  DeforestationTablePartial,
} from './partials';

export const DeforestationView: FC = () => {
  const [months, setMonths] = useStore.deforestationMonths();
  const [monthsText, setMonthsText] = useState<string>(months.toString());

  const handleInput = () => {
    const monthsNum = Number(monthsText);
    const validMonths = !isNaN(monthsNum) && monthsNum > 0 ? monthsNum : months;
    setMonths(validMonths);
    setMonthsText(validMonths.toString());
  };

  return (
    <div className="pb-8">
      <div className="flex items-center justify-end gap-2">
        <label htmlFor="month-input">Calculate by months:</label>
        <Input
          id="month-input"
          className="max-w-[80px] text-center"
          value={monthsText}
          onChange={(e) => setMonthsText(e.target.value)}
          onBlur={handleInput}
          onKeyDown={(e) => e.key === 'Enter' && handleInput()}
        />
      </div>

      <div className="mt-4 grid grid-cols-1 gap-4 md:grid-cols-2">
        <DeforestationPercentPartial />
        <DeforestationAreaPartial />
      </div>

      <DeforestationTablePartial className="mt-8" />
    </div>
  );
};
