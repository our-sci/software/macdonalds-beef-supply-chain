import React, { FC, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Map, Marker, Popup } from 'react-map-gl';
import { MapRef } from 'react-map-gl/dist/esm/mapbox/create-ref';
import { useStore } from 'config/store';
import { LngLatBounds } from 'mapbox-gl';
import { Farm } from 'models/Farm';
import { Pic } from 'models/Pic';
import 'mapbox-gl/dist/mapbox-gl.css';

const toLngLat = ({ lat, lon }: Pic): [number, number] => [lon, lat];

export const MapView: FC = () => {
  const mapRef = useRef<MapRef>(null);
  const [farms] = useStore.farms();
  const [pics] = useStore.pics();
  const [selectedPics] = useStore.selectedPics();
  const [marker, setMarker] = useState<Pic>();

  const [picDetails, selectedPicDetails] = useMemo<Pic[][]>(() => {
    const picDetails = farms.flatMap((farm) => farm.picDetails);
    return [pics, selectedPics].map((item) =>
      item.map((pic) => picDetails.find((detail) => detail.pic === pic) as Pic).filter(Boolean),
    );
  }, [farms, pics, selectedPics]);

  const markerFarm = useMemo<Farm | undefined>(
    () => (marker ? farms.find((farm) => farm.pic.includes(marker.pic)) : undefined),
    [farms, marker],
  );

  const fitMap = useCallback(() => {
    mapRef.current?.resize();

    const [first, ...rest] = selectedPicDetails.length === 0 ? picDetails : selectedPicDetails;
    if (!first) {
      return;
    }

    const bounds = new LngLatBounds(toLngLat(first), toLngLat(first));
    rest.forEach((pic) => {
      bounds.extend(toLngLat(pic));
    });
    mapRef.current?.fitBounds(bounds, { padding: 100, maxZoom: 16 });
  }, [picDetails, selectedPicDetails]);

  useEffect(() => {
    fitMap();
  }, [fitMap]);

  useEffect(() => {
    const resize = () => {
      mapRef.current?.resize();
    };

    window.addEventListener('resize', resize);

    return () => {
      window.removeEventListener('resize', resize);
    };
  }, []);

  return (
    <div className="h-full w-full">
      <Map
        ref={mapRef}
        mapboxAccessToken={import.meta.env.VITE_MAPBOX_ACCESS_TOKEN}
        mapStyle="mapbox://styles/mapbox/satellite-v9"
        initialViewState={{ zoom: 9 }}
        onLoad={fitMap}
      >
        {picDetails.map((pic) => (
          <Marker
            key={pic.pic + selectedPics.includes(pic.pic)}
            longitude={pic.lon}
            latitude={pic.lat}
            anchor="bottom"
            style={{ cursor: 'pointer' }}
            color={selectedPics.includes(pic.pic) ? '#1d4ed8' : '#60a5fa'}
            onClick={(e) => {
              e.originalEvent.stopPropagation();
              setMarker(pic);
            }}
          />
        ))}
        {marker && (
          <Popup
            longitude={marker.lon}
            latitude={marker.lat}
            anchor="bottom"
            offset={60}
            closeButton={false}
            onClose={() => setMarker(undefined)}
          >
            <p>PIC: {marker.pic}</p>
            {markerFarm && <p>{`${markerFarm.name} has ${markerFarm.pic.length} PIC(s)`}</p>}
          </Popup>
        )}
      </Map>
    </div>
  );
};
