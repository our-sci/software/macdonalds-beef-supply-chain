import React, { FC, HTMLAttributes, useState } from 'react';
import clsx from 'clsx';

interface ButtonGroupProps extends Omit<HTMLAttributes<HTMLDivElement>, 'onClick'> {
  items: string[];
  onClick?: (item: string, index: number) => void;
  children?: never;
}

export const ButtonGroup: FC<ButtonGroupProps> = ({ items, onClick, className, ...rest }) => {
  const [active, setActive] = useState<number>(0);

  const handleClick = (item: string, index: number) => () => {
    setActive(index);
    onClick?.(item, index);
  };

  return (
    <div
      className={clsx(className, 'inline-flex shadow-md hover:shadow-lg focus:shadow-lg')}
      role="group"
      {...rest}
    >
      {items.map((item, index) => (
        <button
          key={index}
          type="button"
          className={clsx(
            'inline-block px-6 py-2.5',
            'hover:bg-blue-700 focus:bg-blue-700 active:bg-blue-800',
            'text-xs font-medium uppercase leading-tight text-white',
            'transition-all ease-in-out focus:outline-none focus:ring-0',
            {
              'bg-blue-600': active !== index,
              'bg-blue-800': active === index,
            },
            {
              'rounded-l': index === 0,
              'rounded-r': index === items.length - 1,
            },
          )}
          onClick={handleClick(item, index)}
        >
          {item}
        </button>
      ))}
    </div>
  );
};
