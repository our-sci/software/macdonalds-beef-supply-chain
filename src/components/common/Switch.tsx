import React, { FC, HTMLAttributes } from 'react';
import { Switch as SwitchImpl } from '@headlessui/react';
import clsx from 'clsx';
import { v4 as uuid } from 'uuid';

interface SwitchProps extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> {
  label?: string;
  labelPosition?: 'left' | 'right';
  checked?: boolean;
  onChange?: (checked: boolean) => void;
}

export const Switch: FC<SwitchProps> = ({
  checked,
  label,
  labelPosition = 'right',
  onChange,
  id = uuid(),
  className,
  ...rest
}) => (
  <div
    className={clsx(className, 'inline-flex items-center gap-4', {
      'flex-row-reverse justify-end': labelPosition === 'left',
    })}
    {...rest}
  >
    <SwitchImpl
      id={id}
      checked={checked}
      onChange={onChange}
      className={clsx(
        'relative h-6 w-12',
        'rounded-full border-2 border-transparent',
        'focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75',
        'inline-flex shrink-0 cursor-pointer',
        'transition-colors duration-200 ease-in-out',
        {
          'bg-blue-400': checked,
          'bg-black/20': !checked,
        },
      )}
    >
      <span
        aria-hidden="true"
        className={clsx(
          'inline-block h-5 w-5 rounded-full bg-white shadow-lg ring-0',
          'pointer-events-none transform transition duration-200 ease-in-out',
          {
            'translate-x-6': checked,
            'translate-x-0': !checked,
          },
        )}
      />
    </SwitchImpl>

    {label && (
      <label className="inline-block cursor-pointer " htmlFor={id}>
        {label}
      </label>
    )}
  </div>
);
