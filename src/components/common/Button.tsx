import React, { ButtonHTMLAttributes, FC, ReactNode } from 'react';
import clsx from 'clsx';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: 'normal' | 'outline' | 'text';
  color?: 'primary' | 'secondary' | 'success' | 'error' | 'warning' | 'info' | 'light' | 'dark';
  round?: 'none' | 'normal' | 'full';
  shadow?: boolean;
  leading?: ReactNode;
  trailing?: ReactNode;
}

export const Button: FC<ButtonProps> = ({
  variant = 'normal',
  color = 'primary',
  round = 'normal',
  shadow,
  leading,
  trailing,
  className,
  disabled,
  children,
  ...rest
}) => (
  <button
    data-mdb-ripple="true"
    data-mdb-ripple-color="light"
    className={clsx(
      className,
      'inline-block h-10 px-6',
      'text-xs font-semibold uppercase',
      'focus:outline-none focus:ring-0',
      'transition-all ease-in-out',
      {
        normal: clsx(
          {
            'text-white': color !== 'light',
            'text-gray-700': color === 'light',
          },
          {
            primary: 'bg-blue-500 hover:bg-blue-600 focus:bg-blue-600 active:bg-blue-700',
            secondary: 'bg-purple-500 hover:bg-purple-600 focus:bg-purple-600 active:bg-purple-700',
            success: 'bg-green-500 hover:bg-green-600 focus:bg-green-600 active:bg-green-700',
            error: 'bg-red-500 hover:bg-red-600 focus:bg-red-600 active:bg-red-700',
            warning: 'bg-yellow-500 hover:bg-yellow-600 focus:bg-yellow-600 active:bg-yellow-700',
            info: 'bg-blue-400 hover:bg-blue-500 focus:bg-blue-500 active:bg-blue-600',
            light: 'bg-gray-200 hover:bg-gray-300 focus:bg-gray-300 active:bg-gray-400',
            dark: 'bg-gray-800 hover:bg-gray-900 focus:bg-gray-900 active:bg-gray-900',
          }[color],
        ),
        outline: clsx(
          'border-2 hover:bg-black/5',
          {
            primary: 'border-blue-500 text-blue-500',
            secondary: 'border-purple-500 text-purple-500',
            success: 'border-green-500 text-green-500',
            error: 'border-red-500 text-red-500',
            warning: 'border-yellow-500 text-yellow-500',
            info: 'border-blue-400 text-blue-400',
            light: 'border-gray-200 text-gray-200',
            dark: 'border-gray-800 text-gray-800',
          }[color],
        ),
        text: clsx(
          'bg-transparent',
          {
            primary: 'text-blue-600 hover:text-blue-700 focus:text-blue-700 active:text-blue-800',
            secondary:
              'text-purple-600 hover:text-purple-700 focus:text-purple-700 active:text-purple-800',
            success:
              'text-green-500 hover:text-green-600 focus:text-green-600 active:text-green-700',
            error: 'text-red-600 hover:text-red-700 focus:text-red-700 active:text-red-800',
            warning:
              'text-yellow-500 hover:text-yellow-600 focus:text-yellow-600 active:text-yellow-700',
            info: 'text-blue-400 hover:text-blue-500 focus:text-blue-500 active:text-blue-600',
            light: 'text-gray-200 hover:text-gray-300 focus:text-gray-300 active:text-gray-400',
            dark: 'text-gray-800 hover:text-gray-900 focus:text-gray-900 active:text-gray-900',
          }[color],
        ),
      }[variant],
      {
        'rounded-none': round === 'none',
        rounded: round === 'normal',
        'rounded-full': round === 'full',
      },
      {
        'shadow-md hover:shadow-lg focus:shadow-lg active:shadow-lg': shadow,
        'pointer-events-none opacity-60': disabled,
      },
    )}
    disabled={disabled}
    {...rest}
  >
    <span className="flex h-full items-center justify-center gap-2">
      {leading}
      {children}
      {trailing}
    </span>
  </button>
);
