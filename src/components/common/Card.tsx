import React, { FC, ReactNode } from 'react';
import clsx from 'clsx';

interface CardProps {
  className?: string;
  header?: string;
  content?: string;
  footer?: string;
  children?: ReactNode;
}

export const Card: FC<CardProps> = ({ className, header, content, footer, children }) => (
  <div
    className={clsx(
      className,
      'h-fit flex-1 shrink-0 space-y-4 rounded-xl bg-green-100 py-3 px-10 text-center text-2xl text-black md:py-6',
    )}
  >
    {header && <p className="whitespace-pre-line">{header}</p>}
    {content && <p className="text-4xl text-lime-400">{content}</p>}
    {footer && <p className="text-xl">{footer}</p>}
    {children}
  </div>
);
