import React, { forwardRef, InputHTMLAttributes, Ref } from 'react';
import clsx from 'clsx';

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  ref?: Ref<HTMLInputElement>;
}

export const Input = forwardRef<HTMLInputElement, InputProps>(({ className, ...rest }, ref) => (
  <input
    ref={ref}
    className={clsx(
      className,
      'form-control inline-block h-10 w-full px-3',
      'text-base font-normal leading-[40px] text-gray-700 focus:text-gray-700',
      'bg-white bg-clip-padding focus:bg-white',
      'rounded-lg border border-gray-300 ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-500 focus:outline-none focus:ring-2',
      'transition-all ease-in-out focus:outline-none',
    )}
    {...rest}
  />
));

Input.displayName = 'Input';
