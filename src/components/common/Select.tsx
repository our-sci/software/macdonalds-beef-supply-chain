import React, { FC, Fragment, HTMLAttributes, ReactNode } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import { CheckIcon, ChevronUpDownIcon } from '@heroicons/react/20/solid';
import clsx from 'clsx';

interface SelectProps extends Omit<HTMLAttributes<HTMLDivElement>, 'onChange'> {
  items: string[];
  value: string;
  getLabel?: (value: string) => ReactNode;
  onChange?: (value: string) => void;
  children?: never;
}

export const Select: FC<SelectProps> = ({
  items,
  value,
  getLabel,
  onChange,
  className,
  ...rest
}) => (
  <Listbox value={value} onChange={onChange}>
    <div
      className={clsx(
        className,
        'relative rounded-lg border border-gray-300',
        'ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-500 focus-within:outline-none focus-within:ring-2',
      )}
      {...rest}
    >
      <Listbox.Button
        className={clsx(
          'relative h-10 w-full rounded-lg bg-white py-2 pl-3 pr-10',
          'text-left sm:text-sm',
          'focus:outline-none focus-visible:border-indigo-500',
          'focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300',
          'cursor-pointer',
        )}
      >
        <span className="block truncate">
          {typeof getLabel === 'function' ? getLabel(value) : value}
        </span>
        <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
          <ChevronUpDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
        </span>
      </Listbox.Button>
      <Transition
        as={Fragment}
        leave="transition ease-in duration-100"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <Listbox.Options
          className={clsx(
            'absolute mt-1 -ml-[3px] max-h-60 w-[calc(100%_+_6px)] bg-white py-1 shadow-md',
            'rounded-md border border-green-300',
            'overflow-auto ring-1 ring-black ring-opacity-5 focus:outline-none',
            'text-base sm:text-sm',
          )}
        >
          {items.map((item) => (
            <Listbox.Option
              key={item}
              className={({ active }) =>
                clsx('relative cursor-pointer select-none py-2 pl-10 pr-4', {
                  'bg-blue-200 text-black': active,
                  'text-gray-900': !active,
                })
              }
              value={item}
            >
              {({ selected }) => (
                <>
                  <span
                    className={clsx('block truncate', {
                      'font-medium': selected,
                      'font-normal': !selected,
                    })}
                  >
                    {typeof getLabel === 'function' ? getLabel(item) : item}
                  </span>
                  {selected && (
                    <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                      <CheckIcon className="h-5 w-5" aria-hidden="true" />
                    </span>
                  )}
                </>
              )}
            </Listbox.Option>
          ))}
        </Listbox.Options>
      </Transition>
    </div>
  </Listbox>
);
