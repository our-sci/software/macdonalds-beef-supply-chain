import React, { FC, HTMLAttributes } from 'react';
import clsx from 'clsx';

interface RatingProps extends HTMLAttributes<HTMLDivElement> {
  value: number;
}

const getStar = (value: number, index: number) =>
  value <= index ? 0 : value <= index + 0.5 ? 0.5 : 1;

export const Rating: FC<RatingProps> = ({ value, className, ...rest }) => {
  if (value > 5 || value < 0) {
    return null;
  }

  return (
    <div
      className={clsx(className, 'inline-flex h-6')}
      {...rest}
      title={Number(value.toFixed(1)).toString()}
    >
      {[...Array(5)].map((_, index) => {
        const star = getStar(value, index);

        return star === 0 ? (
          <svg key={index} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g>
              <path
                d="M22 9.24L14.81 8.62L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27L18.18 21L16.55 13.97L22 9.24ZM12 15.4L8.24 17.67L9.24 13.39L5.92 10.51L10.3 10.13L12 6.1L13.71 10.14L18.09 10.52L14.77 13.4L15.77 17.68L12 15.4Z"
                fill="currentColor"
              />
            </g>
          </svg>
        ) : star === 0.5 ? (
          <svg key={index} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g>
              <path
                d="M22 9.24L14.81 8.62L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27L18.18 21L16.55 13.97L22 9.24ZM12 15.4V6.1L13.71 10.14L18.09 10.52L14.77 13.4L15.77 17.68L12 15.4Z"
                fill="currentColor"
              />
            </g>
          </svg>
        ) : (
          <svg key={index} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g>
              <path
                d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z"
                fill="currentColor"
              />
            </g>
          </svg>
        );
      })}
    </div>
  );
};
